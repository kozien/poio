#ifndef KLASY_SPECTRUM_H_
#define KLASY_SPECTRUM_H_

#include "Sequence.h"
#include <string.h>
#include <iostream>

using namespace std;

template <class T, int len = 10>
class Spectrum : public Sequence<T, len>{
private:
	bool scaling; //rodzaj skali (liniowa lub logarytmiczna)


public:

	Spectrum(bool scaling ,
			int channelNr, string unit, double resolution,
			string device, string description, long int date)
			: Sequence<T, len>(channelNr, unit, resolution, device, description, date),

			  scaling(scaling) {};

	virtual ~Spectrum() {};

	string to_string(){

		return Sequence<T, len>::to_string() + "\n Spectrum [Spectrum=" + ::to_string(scaling) +
				"]";
	}

	void Info(){
		cout << to_string() << endl;
	}
};


#endif /* KLASY_SPECTRUM_H_ */
