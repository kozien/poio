#ifndef KLASY_ALARM_H_
#define KLASY_ALARM_H_

#include "Packet.h"
#include <string.h>
#include <iostream>

using namespace std;


template <class T>
class Alarm : public Packet{
protected:
	int channelNr; 	    //numer kanalu
	T treshold;       //wartosc ktorej przekroczenie powoduje sygnalizacje alarmu
	int direction;		//kierunek zmiany (0 - dowolny, -1 -w dol , 1 - w gore)
public:
	Alarm(int channelNr, T treshold, int direction,
			string device, string description, long int date)
	: Packet(device, description, date), channelNr(channelNr), treshold(treshold), direction(direction)
{};

	virtual ~Alarm() {};

	 string to_string(){
		 string out_string = Packet::to_string() + "\n";

		 		out_string += "\n Alarm [channelNr= " + ::to_string(channelNr) +
		 				", treshold=" + ::to_string(treshold)
		 				+ ", direction= " + ::to_string(direction) + "\n" ;

		 		return out_string + "\n";
	 }

	void Info(){
	cout << to_string() << endl;
	}

};

#endif /* KLASY_ALARM_H_ */
