#ifndef KLASY_SEQUENCE_H_
#define KLASY_SEQUENCE_H_

#include <string.h>
#include "Packet.h"
#include <iostream>

using namespace std;

template <class T, int len = 10> class Sequence : public Packet{ //moze byc class badz typename
 //nazwa klasy musi byc taka sama jak nazwa naglowka
protected:
	int channelNr; 	    //numer kanalu
	string unit;        //jednostka mierzonej wielkosci
	double resolution;  //rozdzielczosc w dziedzinie czasu lub innej
	T buffer[len];		//tablice danych typu T
public:
	Sequence(int channelNr, string unit, double resolution, string device, string description, long int date)
	: Packet(device, description, date), channelNr(channelNr), unit(unit), resolution(resolution) {};

	virtual ~Sequence() {};

	T& operator [](int i){              //operator indeksowania
		if(i<0 || i>=len)
		throw i; //return z {} oznacza ze zwracamy pusty obiekt danego typu

		return buffer[i];

	}

	 virtual string to_string(){
		string out_string = Packet::to_string(); //wywolanie z clasy packet

		out_string += "\n Sequence [channelNr= " + ::to_string(channelNr) +
				", unit=" + unit
				+ ", resolution= "
				+ ::to_string(resolution) + + "]" + "\n" ;

		for(int i=0; i<len; i++){

			out_string += "\n buffer[" + ::to_string(i) + "]=" + ::to_string(buffer[i]) ;
		}

		return out_string ;
	}

};



#endif /* KLASY_SEQUENCE_H_ */
