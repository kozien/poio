#ifndef KLASY_PACKET_H_
#define KLASY_PACKET_H_

#include <string.h>

#include <iostream>
using namespace std;

class Packet {
protected:
	string device; //nazwa urzadzenia bedacego zrodlem danych
	string description; //opis slowny danych
	long int date; //data rozumiana jako ilosc sekund od poczatku czas

public:
	Packet(string device, string description, long int date)
	: device(device), description(description), date(date) {};

	virtual ~Packet() {};

	virtual void Info() = 0; //gdy bedzie chociaz jedna taka delkaracja, klasa bedzie nie kompletna i dlatego jest to klasa abstrakcyjna- klasa abstarkcyjna jest po to zeby mozna bylo z niej dziedziczyc


	virtual string to_string(){
		return "\n Packet [device= " + device + ", description=" + description + ", date=" + ::to_string(date) + "]" + "\n"; //to nie jest ta sama funcja co ja napisalismy, to jest funkcja z std::
	}
};


#endif /* KLASY_PACKET_H_ */
