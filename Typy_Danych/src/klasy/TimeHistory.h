#ifndef KLASY_TIMEHISTORY_H_
#define KLASY_TIMEHISTORY_H_

#include "Sequence.h"
#include <string.h>
#include <iostream>

using namespace std;

template <class T, int len = 10>
class TimeHistory : public Sequence<T, len>{
private:
	double sensitivity; // reprezentujacy czulosc napieciowa


public:

	TimeHistory(double sensitivity,
	int channelNr, string unit, double resolution,
	string device, string description, long int date)
	: Sequence<T, len>(channelNr, unit, resolution,device, description, date),sensitivity(sensitivity) {};

	virtual ~TimeHistory() {};

	string to_string(){

		return Sequence<T, len>::to_string() + "\n TimeHistory [sensitivity=" + ::to_string(sensitivity) +
				"]";
	}

	void Info(){
		cout << to_string() << endl;
	}
};


#endif /* KLASY_TIMEHISTORY_H_ */
