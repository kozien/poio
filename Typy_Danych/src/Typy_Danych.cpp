#include <iostream>
using namespace std;

#include <time.h>

//#include "klasy/Packet.h"
#include "klasy/TimeHistory.h"
#include "klasy/Spectrum.h"
#include "klasy/Alarm.h"

int main() {
	cout << "TIMEHISTORY" << endl;
	TimeHistory<int, 5> th1(0.1, 1, "m/s", 0.01, "urzadzenie 1","to jest test", time(NULL));

	cout << "Pierwsze wywolanie to_string [TimeHistory]" << endl;
	cout << th1.to_string() << endl;

	int index = 0;

	try{				//obsuga wyjatkow - tu warunek wyjatku, jesli nie da rady to robi catch
		while(true){
			cout << "Aktualny indeks= " << index << endl;
			th1[index++] = 0;
		}
	}
	catch (int&e){
		cout << "Zly indeks: " << e << endl; //
	}

	cout << endl;

	cout << "Drugie wywolanie to_string [TimeHistory]" << endl;
	cout << th1.to_string() << endl;

	cout << "SPECTRUM" << endl;
	Spectrum<int, 5> sp1(true, 1, "m/s", 0.01, "urzadzenie 1","to jest test", time(NULL));

	try{
		sp1[1] = 1.23;
		sp1[2] = 2.43;
		sp1[10] = 10.23;
	}
	catch(int &e){
		cout << "Spectrum -> zly indeks: " << e << endl;
	}
	cout << sp1.to_string() << endl;
	cout << endl;
	cout << "ALARM" << endl;

	Alarm<double> a1(1, 1500, -1, "urzadzenie 1","to jest test", time(NULL));

	cout << a1.to_string() << endl;

	return 0;
}
