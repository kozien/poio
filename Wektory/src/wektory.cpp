#include <iostream>
using namespace std;

#include <vector> // tablice dynamiczne - wszystykie typy sa szablonowe tzn lista vectory itp

#include <time.h>

int main() {

	int tab_int[5];
	int tab_int_2[] = {1, 3, 6, 7};
	int tab_int_3[5] = {}; //konstruktor domyslny

	for(int i = 0; i<5; i++){
		cout << "tab int[" << i << "]=" << tab_int[i] << endl;
	}
	cout << endl ;

	for(int i = 0; i<5; i++){
			cout << "tab int_2[" << i << "]=" << tab_int_2[i] << endl;
		}
	cout << endl ;

	for(int i = 0; i<5; i++){
			cout << "tab int_3[" << i << "]=" << tab_int_3[i] << endl;
		}
	cout << endl ;

	vector<int> v1(5); //podajemy w ()
	vector<int> v2 = {1, 3, 6, 7};
	vector<int> v3;

	//vector<int> v4, v5 = {1, 2};

	cout << "v1 max size = " << v1.max_size() << endl; //vektory maja duzo metod
	cout << "v2 max size = " << v2.max_size() << endl;
	cout << "v3 max size = " << v3.max_size() << endl;
	cout << endl ;

	cout << "v1 size = " << v1.size() << endl; //vektory maja duzo metod
	cout << "v2 size = " << v2.size() << endl;
	cout << "v3 size = " << v3.size() << endl;
	//cout << "v4 size = " << v4.size() << endl;
	//cout << "v5 size = " << v5.size() << endl;
	cout << endl ;

	for(unsigned int i = 0; i < v1.size(); i++){
				cout << "v1[" << i << "]=" << v1[i] << endl;
			}
	cout << endl;

	for(unsigned int i = 0; i < v2.size(); i++){
				cout << "v2[" << i << "]=" << v2[i] << endl;
				}
	cout << endl;

	for(unsigned int i = 0; i < v3.size(); i++){
				cout << "v3[" << i << "]=" << v3[i] << endl;
				}
	cout << endl;

	cout << "Pierwszy element wektora 2=" << v2.front() << endl;
	cout << "Ostatni element wektora 2=" << v2.back() << endl;

	cout << "PUSH" << endl;
	v2.push_back(12);
	v2.push_back(2);
	v2.push_back(6);
	v2.push_back(4);

	cout << "Rozmiar wektora 2: " << v2.size() << endl;
	cout << "Elementy wektora 2: " << endl;
	for(unsigned int i = 0; i < v2.size(); i++){
					cout << "v2[" << i << "]=" << v2[i] << endl;
					}
	cout << endl;

	cout << "POP" << endl;
	v2.pop_back(); //usuwa ostatni element wektora
	v2.pop_back();
	v2.pop_back();

	cout << "Rozmiar wektora 2: " << v2.size() << endl;
	cout << "Elementy wektora 2: " << endl;
	for(unsigned int i = 0; i < v2.size(); i++){
					cout << "v2[" << i << "]=" << v2[i] << endl;
					}
	cout << endl;
	cout << "RESIZE w dol: " << endl;
	v2.resize(2); //zmiana wieklosci wektora -- zostaja 2 na poczatku

	cout << "Rozmiar wektora 2: " << v2.size() << endl;
	cout << "Elementy wektora 2: " << endl;
	for(unsigned int i = 0; i < v2.size(); i++){
					cout << "v2[" << i << "]=" << v2[i] << endl;
					}
	cout << endl;

	cout << "RESIZE w gore: " << endl;
	v2.resize(5); //zmiana wieklosci wektora w gore -- reszte miejsc wypelni sie zerami

	cout << "Rozmiar wektora 2: " << v2.size() << endl;
	cout << "Elementy wektora 2: " << endl;
	for(unsigned int i = 0; i < v2.size(); i++){
					cout << "v2[" << i << "]=" << v2[i] << endl;
					}
	cout << endl;

	cout << "CLEAR" << endl;
	v2.clear(); //usuwa wszystkie elementy

	cout << "Rozmiar wektora 2: " << v2.size() << endl;
	cout << "Elementy wektora 2: " << endl;
	for(unsigned int i = 0; i < v2.size(); i++){
					cout << "v2[" << i << "]=" << v2[i] << endl;
					}
	cout << endl;

	srand(time(NULL));
	for(int i =0; i < 7; i++)
		v2.push_back(rand() % 100);

	cout << "Rozmiar wektora 2: " << v2.size() << endl;
	cout << "Elementy wektora 2: " << endl;
	for(unsigned int i = 0; i < v2.size(); i++){
					cout << "v2[" << i << "]=" << v2[i] << endl;
					}
	for(int el : v2)
		cout << el << ", ";
	cout << endl << endl;

	cout << "Iteratory" << endl; //uogolnienie wskaznikow
	//vector<int>::iterator it1 = v2.begin(); //wskaznik na pierwszy element
	vector<int>::iterator it1 = v2.begin() + 2; // na 3 element
	cout << "Trzeci od poczatku element wektora v2: " << *it1 << endl ; //*it1 to wartosc
	//cout << "Adres it1= " << it1.base() << endl;
	it1 = v2.end() -1; //ostatni element bo end jest na adres za ostatnim
	cout << "Ostatni element wektora v2: " << *it1 << endl;
	it1 = v2.begin() + v2.size() -1;
	cout << "Ostatni element wektora v2: " << *it1 << endl;

	//cout << "Adres it1= " << it1.base() << endl;

	vector<int>::reverse_iterator rit1 = v2.rbegin() + 2;
	cout << "Trzeci od konca element wektora v2: " << *rit1 << endl ;
	rit1 = v2.rend() - 1;
	cout << "Pierwszy element wektora v2: " << *rit1 << endl;
	rit1 = v2.rbegin() + v2.size() -1;
	cout << "Pierwszy element wektora v2: " << *rit1 << endl;

	cout << "AKtualny rozmiare wektora 2: " << v2.size() << endl;
	cout << "Elementy wektora 2: " << endl;
	for(int el : v2)
		cout << el << ", ";
	cout << endl;
	cout << "ERASE " << endl;
	v2.erase(v2.begin() +1); //usuwamy 1 element

	cout << "Elementy wektora 2: " << endl;
	for(int el : v2)
		cout << el << ", ";
	cout << endl;
	cout << "ERASE 2 " << endl;
	v2.erase(v2.begin(), v2.begin() + 2); //usuwamy 2 elementy - tu chodzi o zakres
	cout << "AKtualny rozmiare wektora 2: " << v2.size() << endl;
	cout << "Elementy wektora 2: " << endl;
	for(int el : v2)
		cout << el << ", ";
	cout << endl;

	cout << "INSERT 1" << endl;
	v2.insert(v2.begin(), 5); //wstawianie 5 na sam poczatek
	cout << "AKtualny rozmiare wektora 2: " << v2.size() << endl;
	cout << "Elementy wektora 2: " << endl;
	for(int el : v2)
		cout << el << ", ";
	cout << endl;

	cout << "INSERT 2" << endl;
	v2.insert(v2.begin() + 2, 3, 7); //wstawianie trzech 7 od miejsca 3
	cout << "AKtualny rozmiare wektora 2: " << v2.size() << endl;
	cout << "Elementy wektora 2: " << endl;
	for(int el : v2)
		cout << el << ", ";
	cout << endl;

	cout << "INSERT 3" << endl;
	v2.insert(v2.end() - 2, 5, {}); //wstawienie piecu zer
	cout << "AKtualny rozmiare wektora 2: " << v2.size() << endl;
	cout << "Elementy wektora 2: " << endl;
	for(int el : v2)
		cout << el << ", ";
	cout << endl;



	return 0;
}
