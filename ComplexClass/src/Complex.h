#ifndef COMPLEX_H_
#define COMPLEX_H_

#include <string>
#include <iostream>
using namespace std;

class Complex {
private:
	string nazwa;
	double real;
	double imag;
	double mod;
	double arg;
public:
	Complex(); //konstruktor pusty
	virtual ~Complex();
	Complex(string nazwa, double real, double imag); //konstruktor 3 param
	Complex(double real, double imag); //konstruktor 2 param
	Complex(const Complex &inna_liczba); //konstruktor kopiuj�cy

	void info();
	void info_bez_nazwy();

	double modul() const;
	double kat();

	void SetName(string nazwa){this->nazwa = nazwa; };
	void SetReal(double real){this->real = real; Przekalkulowanie();};
	void SetImag(double imag){this->imag = imag; Przekalkulowanie();};

	string Get_name(){return nazwa;};
	double Get_real(){return real;};
	double Get_imag(){return imag;};

	void input_from_console();

	void Przekalkulowanie()
	{
	    this->modul();
	    this->kat();
	}


	int zwroc_real(){return real;}
	int zwroc_imag(){return imag;}

//--- OPERATORY---------------

	Complex operator+(const Complex& r);
	Complex operator-(const Complex& r);
	Complex operator*(const Complex& r);
	Complex operator/(const Complex& r);

	friend bool operator>(const Complex  &l, const Complex  &r);
	friend bool operator<(const Complex  &l, const Complex  &r);
	friend bool operator>=(const Complex  &l, const Complex  &r);
	friend bool operator<=(const Complex  &l, const Complex  &r);
	friend bool operator==(const Complex  &l, const Complex  &r);
	friend bool operator!=(const Complex  &l, const Complex  &r);

	Complex conj();
	Complex reciprocal();

};

#endif /* COMPLEX_H_ */
