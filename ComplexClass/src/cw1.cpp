#include <iostream>
using namespace std;
#include "Complex.h"

int main() {
	//zad 1

	Complex nl1;
	Complex nl2("Liczba_2", 10.2, 5.3);
	Complex nl3(12.6, 6.2);
	Complex nl4(nl2);

	Complex *jl1 = 0, *jl2=0, *jl3 = 0 ,*jl4 = 0;
	jl1 = new Complex; //konst pusty
	jl2= new Complex("Liczba 2",5.2,1.0); //kosnt 3 parametrwoy
	jl3= new Complex(2.0,3.0);
	jl4= new Complex(*jl2);

	jl1->info();
	jl2->info();
	jl3->info();
	jl4->info();

	nl1.info();
	nl2.info();
	nl3.info();
	nl4.info();
	cout << "-------------------------------------------"<< endl;

	//zad 2

	Complex liczby[3] = {{"Punkt 1",1.0,1.0}, {"Punkt 2",2.0,2.0}, {"Punkt 3",3.0,3.0}};

	for (int i = 0; i<3; i++){
		liczby[i].info();
	}

	//zad 3

	Complex l1("Liczba 1", 4.0, 3.0);
	Complex l2("Liczba 2", 5.0, 2.0);
	Complex suma,roznica,mnozenie,dzielenie;

	suma = l1 + l2;
	roznica = l1 - l2;
	mnozenie = l1 * l2;
	dzielenie = l1 / l2;

	suma.info();
	roznica.info();
	mnozenie.info();
	dzielenie.info();

	//zad4

if(operator>(l1,l2))
	cout << "l1 jest wieksze "<< endl;
else
	cout << "l2 jest wieksze " << endl;

if(l1<l2)
	cout << "l2 jest wieksze "<< endl;
else
	cout << "l1 jest wieksze " << endl;
if(l1<=l2)
	cout << "l1 jest mniejsze badz rowne "<< endl;
else
	cout << "l1 jest wieksze  " << endl;
if(l1>=l2)
	cout << "l1 jest wieksze badz rowne "<< endl;
else
	cout << "l1 jest mniejsze " << endl;
if(l1==l2)
	cout << "l1 jest rowne l2 "<< endl;
else
	cout << "liczby nie sa rowne" << endl;
if(l1!=l2)
	cout << "liczby sa rozne "<< endl;
else
	cout << "liczby sa rowne " << endl;



	if(jl1) delete jl1;
	if(jl2) delete jl2;
	if(jl3) delete jl3;
	if(jl4) delete jl4;

	return 0;
}
