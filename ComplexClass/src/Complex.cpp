#include "Complex.h"
#include <iostream>
#include <math.h>

Complex::Complex() {
	nazwa="Brak nazwy";
	real=0.0;
	imag=0.0;
	mod = modul();
	arg = kat();
	cout << "utworzono pusty" << endl;
}

Complex::Complex(string nazwa, double real, double imag) {
	this->nazwa=nazwa;
	this->real=real;
	this->imag=imag;
	mod = modul();
	arg = kat();
	cout << "utworzono 3 parametrowy" << endl;
}

Complex::Complex(double real, double imag) {
		this->real=real;
		this->imag=imag;
		mod = modul();
		arg = kat();
		cout << "utworzono 2 parametrowy" << endl;
}

Complex::Complex(const Complex &inna_liczba){
	this->nazwa= inna_liczba.nazwa;
	this->real= inna_liczba.real;
	this->imag= inna_liczba.imag;
	this->mod = inna_liczba.mod;
	this->arg = inna_liczba.arg;
	cout << "skopiowano" << endl;
}

Complex::~Complex() {
	cout << "Zniszczono"<<endl;
}

double Complex::modul() const {
	return sqrt(pow(real, 2.0) + pow(imag, 2.0)); //pow to potega
}

double Complex::kat(){
	return atan2(imag, real) *180.0/M_PI; //ma dwa paramtery i sprawdza znaki jak w matlabie
}

void Complex::info() {
cout << "Nazwa:" << nazwa << endl;
cout << "Real:" << real << endl;
cout << "Imag:" << imag << endl;
cout << "Modul:" << mod << endl;
cout << "Kat:" << arg << endl;
cout << "Adres : 0x" << this << endl;
}

void Complex::info_bez_nazwy() {
cout << "Real:" << real << endl;
cout << "Imag:" << imag << endl;
cout << "Modul:" << mod << endl;
cout << "Kat:" << arg << endl;
cout << "Adres : 0x" << this << endl;
}

void Complex::input_from_console(){
	cout << "Podaj nazwe liczby" << endl;
	cin >> nazwa;
	cout << "Podaj cz. rzeczywista" << endl;
	cin >> real;
	cout << "Podaj cz. urojona" << endl;
	cin >> imag;
	mod= modul();
	arg = kat();
}

Complex Complex::operator+(const Complex& r){
	Complex res("Suma", real + r.real, imag + r.imag);
	return res;
}

Complex Complex::operator-(const Complex& r){
	Complex res("Roznica", real - r.real, imag - r.imag);
	return res;
}

Complex Complex::operator*(const Complex& r){
	Complex res("Iloczyn", real * r.real, imag * r.imag);
	return res;
}

Complex Complex::operator/(const Complex& r){
	Complex res("Iloraz", real / r.real, imag / r.imag);
	return res;
}



bool operator>(const Complex &l, const Complex &r){
	return l.modul() > r.modul() ? true : false;
}

bool operator<(const Complex  &l, const Complex  &r){
	return l.modul() < r.modul() ? true : false;
}

bool operator>=(const Complex  &l, const Complex  &r){
	return l.modul() >= r.modul() ? true : false;
}

bool operator<=(const Complex  &l, const Complex  &r){
	return l.modul() <= r.modul() ? true : false;
}

bool operator==(const Complex  &l, const Complex  &r){
	return l.modul() == r.modul() ? true : false;
}

bool operator!=(const Complex  &l, const Complex  &r){
	return l.modul() != r.modul() ? true : false;
}


Complex Complex::conj(){
	Complex con("Sprzezenie", real, imag * (-1));
	return con;
}

Complex Complex::reciprocal(){
	Complex recip("Odwrotnosc", real * (-1), imag * (-1));
	return recip;
}




