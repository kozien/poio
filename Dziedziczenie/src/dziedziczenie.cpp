#include <iostream>
using namespace std;

#include "punkty/P2D.h"
#include "punkty/P3D.h"
#include "punkty/P4D.h"
//using namespace P2D; // mozna usunac z cout p2d

int main() {

	P2D p1(1,1), p2(1.7, 3.9), p3;

	p1.Info();
	p2.Info();
	p3.Info();
	cout <<endl;

	cout << "Odleglosc P1-P2 " << p1.distance(p2) << endl;
	cout << "Odleglosc P1-P3 " << p1.distance(p3) << endl;
	cout <<endl;

	cout << "Odleglosc P1-P2 " << P2D::distance_to(p1,p2) << endl;
	cout << "Odleglosc P1-P3 " << P2D::distance_to(p1,p3) << endl;

	P3D p4(1, 1, 1), p5;

	cout << "P4:X =" << p4.get_X() << "Y =" << p4.get_Y() << "Z =" << p4.get_Z() << endl;

	p4.Info();
	p5.Info();
	cout <<endl;

	cout << "Odleglosc P4-P2 " << p4.distance(p2) << endl;
	cout <<endl;
	cout << "Odleglosc P4-P5 " << P3D::distance_to(p4,p5) << endl;

	P4D p6(1, 1, 1, 1), p7;

	cout << "P6:X =" << p6.get_X() << "Y =" << p6.get_Y() << "Z =" << p6.get_Z() << "T =" << p6.get_T() << endl;
	cout << "P7:X =" << p7.get_X() << "Y =" << p7.get_Y() << "Z =" << p7.get_Z() << "T =" << p7.get_T() << endl;
	p6.Info();
	p7.Info();
	cout <<endl;

	cout << "Odleglosc P6-P5 " << p6.distance(p5) << endl;
	cout <<endl;
	cout << "Odleglosc P6-P7 " << P4D::distance_to(p6,p7) << endl;

	return 0;
}
