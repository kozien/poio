#include "P3D.h"

#include <iostream>
using namespace std;

void P3D::Info(){

	cout << "Punkt 3D, x=" << X << ", y=" << Y << "z=" << Z << endl;
}

double P3D::distance_to(P3D p1, P3D p2){

	return sqrt(pow(p1.X -p2.X, 2) + pow(p1.Y -p2.Y, 2) + pow(p1.Z -p2.Z, 2));
}

double P3D::distance(P3D other){

	return sqrt(pow(X - other.X, 2) + pow(Y - other.Y,2) + pow(Z - other.Z,2));
}

double P3D::distance(P2D other){

	return sqrt(pow(X - other.get_X(), 2) + pow(Y - other.get_Y(),2) + pow(Z ,2));
}

