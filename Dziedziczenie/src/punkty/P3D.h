#ifndef PUNKTY_P3D_H_
#define PUNKTY_P3D_H_

#include "P2D.h"

#include <math.h>
class P3D : public P2D {
	protected:
	double Z;


public:
	P3D(double x, double y, double z) : P2D(x, y), Z(z) {};// przy tworzeniu 3 arg pounktu uzywamy 2 arg
	P3D() : P2D(0.0, 0.0), Z(0.0) {}; //konstruktor zerowy

	virtual ~P3D() {};

	double get_Z() {return Z; }; //gettery settery

    virtual void Info ();

    static double distance_to(P3D p1, P3D p2); //ogarnac po co static
    static double distance_to(P3D p1, P2D p2); //przeciazenie statica

    double distance(P3D other);
    double distance(P2D other); // to jest przeciazenie -- ogarnac przeciazenie

};

#endif /* PUNKTY_P3D_H_ */
