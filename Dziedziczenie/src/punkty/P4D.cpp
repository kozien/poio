#include "P4D.h"
#include <iostream>
using namespace std;

void P4D::Info(){

	cout << "Punkt 4D,  x=" << X << ", y=" << Y << "z=" << Z << "T="<< T << endl;
}

double P4D::distance_to(P4D p1, P4D p2){

	return sqrt(pow(p1.X -p2.X, 2) + pow(p1.Y -p2.Y, 2) + pow(p1.Z -p2.Z, 2) + pow(p1.T -p2.T, 2));
}
double P4D::distance(P4D other){

	return sqrt(pow(X - other.X, 2) + pow(Y - other.Y,2) + pow(Z - other.Z,2) + pow(T - other.T,2));
}
double P4D::distance(P3D other){

	return sqrt(pow(X - other.get_X(), 2) + pow(Y - other.get_Y(),2) + pow(Z - other.get_Z(),2) + pow(T ,2));
}

double P4D::distance(P2D other){

	return sqrt(pow(X - other.get_X(), 2) + pow(Y - other.get_Y(),2) + pow(Z ,2)+ pow(T ,2));
}
