/*
 * P2D.h
 *
 *  Created on: 8 kwi 2020
 *      Author: student
 */

#ifndef PUNKTY_P2D_H_
#define PUNKTY_P2D_H_

class P2D {
protected:
	double X, Y;

public:
	P2D(double x, double y ) : X(x), Y(y) {}; //konstruktor - duze x przyjmie male x
	P2D() : X(0.0), Y(0.0) {}; //konstruktor zerowy (inna skladnia troche, moznaby to normalnie zrobic ale w pliku ccpp)

	double get_X() {return X; }; //gettery settery
	double get_Y() {return Y; };

	virtual void Info(); // bo rozszerzamy funckje info w p3d

	double distance(P2D other);

	static double distance_to(P2D p1, P2D p2);

	virtual ~P2D() {};
};

#endif /* PUNKTY_P2D_H_ */
