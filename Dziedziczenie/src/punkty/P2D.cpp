/*
 * P2D.cpp
 *
 *  Created on: 8 kwi 2020
 *      Author: student
 */

#include "P2D.h"
#include <math.h>
#include <iostream>
using namespace std;

void P2D::Info(){

	cout << "Punkt 2D, x=" << X << ", y=" << Y << endl;
}

double P2D::distance_to(P2D p1, P2D p2){

	return sqrt(pow(p1.X -p2.X, 2) + pow(p1.Y -p2.Y, 2));
}

double P2D::distance(P2D other){

	return sqrt(pow(X - other.X, 2) + pow(Y - other.Y,2));
}
