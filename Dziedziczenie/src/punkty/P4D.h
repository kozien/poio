#ifndef PUNKTY_P4D_H_
#define PUNKTY_P4D_H_
#include "P3D.h"
#include "math.h"

class P4D : public P3D{
	protected:
	double T;
public:
	P4D(double x, double y, double z, double t) : P3D(x, y, z), T(t) {};
	P4D() : P3D(0.0, 0.0, 0.0), T(0.0) {}; //konstruktor zerowy

	virtual ~P4D() {};

	double get_T() {return T; }; //gettery settery

	void Info ();

	static double distance_to(P4D p1, P4D p2);
	static double distance_to(P4D p1, P3D p2);
	static double distance_to(P4D p1, P2D p2);

	double distance(P4D other);
	double distance(P3D other);
	double distance(P2D other);
};

#endif /* PUNKTY_P4D_H_ */
