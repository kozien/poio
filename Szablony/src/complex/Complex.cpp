#include "Complex.h"
#include <math.h>
#include <iostream>

using namespace std;

bool Complex::operator>(Complex &other){
		if(c_abs() > other.c_abs())
		return true;

return false;
}

bool Complex::operator<(Complex &other){
		if(c_abs() < other.c_abs())
		return true;

return false;
}
ostream & operator<<(ostream &console_out, const Complex &number){
	return console_out << number.real << "+" << number.imag << "i";
}

double Complex::c_abs(){
	return sqrt(pow(real,2) + pow(imag,2));
}

