#ifndef COMPLEX_H_
#define COMPLEX_H_
#include <iostream>

using namespace std;

class Complex {
private:
	double real;
	double imag;

public:

	Complex(double re, double im) : real(re), imag(im) {};
	Complex() : real(0), imag(0) {};

	virtual ~Complex() {};

	bool operator>(Complex &other);
	bool operator<(Complex &other);

	friend ostream & operator<<(ostream &console_out, const Complex &number);

private:
	double c_abs();

};

#endif
