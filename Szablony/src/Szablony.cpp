#include <iostream>
using namespace std;

#include "complex/Complex.h"
#include "sort/min_max.h"
#include "sort/sort.h"
#include <time.h>

int main() {

	int  a_int = 2, b_int = 9;

	double a_double = 17.6, b_double = 9.1;

	Complex a_complex, b_complex(4, 6);

	cout << "Maximum z int " << maximum(a_int, b_int) << endl;
	cout << "Maximum z double" << maximum(a_double, b_double) << endl;
	cout << "Maximum z Complex" << maximum(a_complex, b_complex) << endl;
	cout << endl;

	cout << "Maximum z 3 int" << minimum(a_int, b_int) << endl;
	cout << "Minimum z double" << minimum(a_double, b_double) << endl;
	cout << "Minimum z Complex" << minimum(a_complex, b_complex) << endl;
	cout << endl;

	cout << "Maximum z 3 int: " << maximum_of_3(1,2,3) << endl;
	cout << "Maximum z 3 int: " << maximum_of_3(1,3,2) << endl;
	cout << "Maximum z 3 int: " << maximum_of_3(3,1,2) << endl;
	cout << "Maximum z 3 int: " << maximum_of_3(2,3,1) << endl;
	cout << "Maximum z 3 int: " << maximum_of_3(1,3,2) << endl;
	cout << "Maximum z 3 int: " << maximum_of_3(14,22,31) << endl;
	cout << endl;

	cout << "Maximum z 3 int: " << maximum(1,2,3) << endl;
	cout << "Maximum z 3 int: " << maximum(1,3,2) << endl;
	cout << "Maximum z 3 int: " << maximum(3,1,2) << endl;
	cout << "Maximum z 3 int: " << maximum(2,3,1) << endl;
	cout << "Maximum z 3 int: " << maximum(1,3,2) << endl;
	cout << "Maximum z 3 int: " << maximum(14,22,31) << endl;
	cout << endl;

	cout << "Maximum z 4 int: " << maximum(1,3,2,6) << endl;
	cout << "Maximum z 4 int: " << maximum(14,22,31,7) << endl;
	cout << endl;

	cout << "Maximum z 5 int: " << maximum(1,3,2,6,8) << endl;
	cout << "Maximum z 5 int: " << maximum(14,22,31,7,8) << endl;
	cout << endl;

	cout << "Maximum z 4 Complex: " << maximum(Complex(1, 1), Complex(2, 2), Complex(3, 3), Complex(4, 4)) << endl;
	cout << endl;

	cout << "Minimum z int " << minimum(a_int, b_int) << endl;
	cout << "Minimum z double" << minimum(a_double, b_double) << endl;
	cout << "Minimum z Complex" << minimum(a_complex, b_complex) << endl;
	cout << endl;

	cout << "Minimum z 3 int" << minimum(a_int, b_int) << endl;
	cout << "Minimum z double" << minimum(a_double, b_double) << endl;
	cout << "Minimum z Complex" << minimum(a_complex, b_complex) << endl;
	cout << endl;

	cout << "Minimum z 3 int: " << minimum_of_3(1,2,3) << endl;
	cout << "Minimum z 3 int: " << minimum_of_3(1,3,2) << endl;
	cout << "Minimum z 3 int: " << minimum_of_3(3,1,2) << endl;
	cout << "Minimum z 3 int: " << minimum_of_3(2,3,1) << endl;
	cout << "Minimum z 3 int: " << minimum_of_3(1,3,2) << endl;
	cout << "Minimum z 3 int: " << minimum_of_3(14,22,31) << endl;
	cout << endl;

	cout << "Minimum z 3 int: " << minimum(1,2,3) << endl;
	cout << "Minimum z 3 int: " << minimum(1,3,2) << endl;
	cout << "Minimum z 3 int: " << minimum(3,1,2) << endl;
	cout << "Minimum z 3 int: " << minimum(2,3,1) << endl;
	cout << "Minimum z 3 int: " << minimum(1,3,2) << endl;
	cout << "Minimum z 3 int: " << minimum(14,22,31) << endl;
	cout << endl;

	cout << "Minimum z 4 int: " << minimum(1,3,2,6) << endl;
	cout << "Minimum z 4 int: " << minimum(14,22,31,7) << endl;
	cout << endl;

	cout << "Minimum z 5 int: " << minimum(1,3,2,6,8) << endl;
	cout << "Minimum z 5 int: " << minimum(14,22,31,7,8) << endl;
	cout << endl;

	cout << "Minimum z 4 Complex: " << minimum(Complex(1, 1), Complex(2, 2), Complex(3, 3), Complex(4, 4)) << endl;
	cout << endl;

	srand(time(NULL));

	sort<int, 5> tab_int;
	//cout << "Liczby int: ";
	//tab_int.print_values();

	int index = 0;
	while(tab_int.set(index, rand() %100)){
		index++;
	}
	cout << "Przygotowano :"<< index << " elementow typu int:";
	tab_int.print_values();
	tab_int.bubble_sort_values();
	cout << "Posortowane int: " ;
	tab_int.print_values();

	cout << endl;

	sort<double, 10> tab_double;
	//cout << "Liczby double: ";
	//tab_double.print_values();

	index = 0;
	while(tab_double.set(index, (rand() %10) + 0.01 *(rand() %100) )){
			index++;
		}
	cout << "Przygotowano :"<< index << " elementow typu double:";
	tab_double.print_values();
	tab_double.bubble_sort_values();
	cout << "Posortowane double: " ;
	tab_double.print_values();

	cout << endl;

	sort<Complex, 5> tab_complex;
	//cout << "Liczby Complex: ";
	//tab_complex.print_values();

	index = 0;
	while(tab_complex.set(index, Complex((rand() %10), rand() %10))){
			index++;
		}
	cout << "Przygotowano :"<< index << " elementow typu Complex:";
	tab_complex.print_values();
	tab_complex.bubble_sort_values();
	cout << "Posortowane Complex: " ;
	tab_complex.print_values();

	return 0;
}
