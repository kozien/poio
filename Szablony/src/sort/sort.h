
#ifndef SORT_SORT_H_
#define SORT_SORT_H_
#include <iostream>

using namespace std;

template <class T, int I=10> class sort{
private:
	T table[I];

public:
	bool set(int i, const T t){
		if((i<0 || (i>= I)))
			return false;

		table[i] = t;
		return true;
	}

	void bubble_sort_values(){

		int i;
		T temp;
		bool was_any_change;

		do{
			was_any_change = false;

		for(i=0; i< (I-1); i++){
			if(table[i] > table[i+1]){

				temp = table[i];
				table[i] = table[i+1];
				table[i+1] = temp;

				was_any_change = true;
				}
			}
		}while(was_any_change);
	}

	void print_values(){

		for(int i =0; i<(I-1); i++)
			cout << table[i] << ",";

		cout << table [I-1] << "," << endl;
	}
};




#endif /* SORT_SORT_H_ */
