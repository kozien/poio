#ifndef MIN_MAX_H_
#define MIN_MAX_H_

template <class T>
T maximum(T liczba_1, T liczba_2){

	return liczba_1 > liczba_2 ? liczba_1 : liczba_2;
}
/*
template <class T>
T maximum(T liczba_1, T liczba_2, T liczba_3){

	return maximum(maximum(liczba_1, liczba_2), liczba_3) ;
}
*/

//minimum tak jak dla maximum


template <class T, class ... Args>
T maximum(T liczba_1, T liczba_2, Args ... args){

	return maximum(maximum(liczba_1, liczba_2), args ...) ;
}


template <class T>
T maximum_of_3(T liczba_1, T liczba_2, T liczba_3){
	if(liczba_1 > liczba_2){
		//1>2
		if(liczba_1 > liczba_3){
			//1>2 && 1>3
			return liczba_1;
		}else{
			//1>2 && 1 < 3
			return liczba_3;
		}
	}else{
		//1<3
		if(liczba_2 > liczba_3){
			//1 <2 && 2>3
			return liczba_2;
		}else{
			//1<2 && 2<3
			return liczba_3;
		}
	}
}

template <class T>
T minimum(T liczba_1, T liczba_2){

	return liczba_1 < liczba_2 ? liczba_1 : liczba_2;
}

template <class T, class ... Args>
T minimum(T liczba_1, T liczba_2, Args ... args){

	return minimum(minimum(liczba_1, liczba_2), args ...) ;
}

template <class T>
T minimum_of_3(T liczba_1, T liczba_2, T liczba_3){
	if(liczba_1 < liczba_2){
		//1<2
		if(liczba_1 < liczba_3){
			//1<2 && 1<3
			return liczba_1;
		}else{
			//1<2 && 1 > 3
			return liczba_3;
		}
	}else{
		//1>3
		if(liczba_2 > liczba_3){
			//1 >2 && 2<3
			return liczba_2;
		}else{
			//1>2 && 2>3
			return liczba_3;
		}
	}
}

#endif /* MIN_MAX_H_ */
